# 申請者
x_employee_code = fields.Char(string="社員番号", default="𛲕 𛲕 𛲕 𛲕")
x_surname = fields.Char(string="氏　　　名", default="𛲕 𛲕 𛲕 𛲕")
x_department_applicant = fields.Char(string="所属部署", default="𛲕 𛲕 𛲕 𛲕")

# 終了対象者情報　（申請者記載）
x_end_date = fields.Date(string="終了日")
x_target_persons_name = fields.Date(string="対象者氏名")
x_current_contract_classification = fields.Char(string="現契約区分", default="𛲕 𛲕 𛲕 𛲕")
x_cooperating_company_name = fields.Char(string="協力会社名", default="𛲕 𛲕 𛲕 𛲕")
x_employee_number = fields.Char(string="社員番号", default="𛲕 𛲕 𛲕 𛲕")
x_department = fields.Char(string="所属部署", default="𛲕 𛲕 𛲕 𛲕")
x_mail_id = fields.Char(string="mailID", default="𛲕 𛲕 𛲕 𛲕")
x_applicants_comment = fields.Char(string="申請者コメント")
# 添付資料
x_attachment = fields.Binary(string="添付資料①")

# 回付状況
x_applicant_name = fields.Char(string="申請者氏名", default="𛲕 𛲕 𛲕 𛲕")
x_applicant_date = fields.Date(string="申請日")

x_chief_name = fields.Char(string="課長氏名", default="𛲕 𛲕 𛲕 𛲕")
x_chief_approval_date = fields.Date(string="課長承認日")
x_Chief_comment = fields.Char(string="コメント")

x_minister_name = fields.Char(string="部長指名", default="𛲕 𛲕 𛲕 𛲕")
x_ministers_recognition_day = fields.Date(string="部長承認日")
x_minister_comment = fields.Char(string="コメント")

x_in_charge_of_general_affairs_section = fields.Char(string="総務課担当", default="𛲕 𛲕 𛲕 𛲕")
x_reception_date = fields.Date(string="受付日")
x_receipt_number = fields.Integer(string="受付番号")
x_comment = fields.Char(string="コメント")

x_responsible_for_the_love_and_enterprise_class = fields.Char(string="情企課担当", default="𛲕 𛲕 𛲕 𛲕")
x_reception_date = fields.Date(string="受付日")
x_in_charge_of_general_comment = fields.Char(string="コメント")

x_circulation_of_related_parties = fields.Char(string="関係者回付")