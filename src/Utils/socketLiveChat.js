const User = require("../Models/user.model");
const err = require("../Errors/index");
const Customer = require("../Models/customer.model");
const { TypeChannel } = require("../Enums");
const Conversation = require("../Models/conversation.model");
const Project = require("../Models/project.model");
const { addMessage } = require("../Controllers/message.controller");
const { addConversation } = require("../Controllers/conversation.controllers");
const Channel = require("../Models/channel.model");
const VaConfig = require("../Models/vaConfig.model");
const { sendMessageToSupperAi } = require("./AiUtils");
module.exports = async (io) => {
  try {
    io.use(async (socket, next) => {
      try {
        const token = socket.handshake.auth.token;
        const projectId = socket?.handshake?.query?.projectId;
        if (projectId) {
          const userId = socket?.handshake?.query?.userId;
          if (!userId) next(err.USER_NOT_FOUND);
          const project = await Project.findOne({ _id: projectId }).populate(
            "channels",
            "type"
          );
          if (!project) next(err.PROJECT_NOT_FOUND);
          const channelLiveChat = project.channels?.find(
            (c) => c.type === TypeChannel.LIVECHAT
          );
          if (channelLiveChat) {
            const vaConfig = await VaConfig.findOne({
              channel: channelLiveChat?._id,
            });
            channelLiveChat.vaConfig = vaConfig;
            socket.channelLiveChat = channelLiveChat;
          }
          socket.project = project;
          socket.customerId = userId;
          next();
        } else if (token) {
          const { user } = await User.verifyJwtToken(token);
          if (!user) next(err.USER_NOT_FOUND);
          socket.user = user;
          next();
        } else {
          next(err.NOT_AUTHORIZED);
        }
      } catch (error) {
        next(error);
      }
    });
    io.on("connection", async (socket) => {
      try {
        socket.on("disconnect", function () {
          //  console.log("out_______________________________",socket.customer);
        });
        if (socket.project) {
          socket.on("create_conversation", async ({ phoneNumber, type }) => {
            if (!socket.channelLiveChat)
              throw new Error(err.CHANNEL_NOT_FOUND.message);
            if (!Object.values(TypeChannel).includes(type))
              throw new Error("Yêu cầu gửi đúng type");
            let customer = await Customer.findOne({ _id: socket.customerId });
            if (!customer) {
              customer = new Customer({ phoneNumber, _id: socket.customerId });
              await customer.save({ new: true });
            }
            let conversation = await Conversation.findOne({
              customer: customer._id,
            });
            if (!conversation) {
              conversation = await addConversation({
                customer: customer._id,
                channel: socket.channelLiveChat?._id,
              });
              io.to(`channel_${socket.channelLiveChat?._id}`).emit(
                "new_conversation",
                {
                  conversation,
                }
              );
            }
            socket.customer = customer;
            socket.conversationId = conversation._id;
            socket.join(`conversation_${socket.conversationId}`);
            io.to(socket.id).emit("notification", {
              type: "success",
              message: "Create conversation success!!!",
            });
          });
        } else if (socket.user) {
          // Kết nối channel
          socket.on("join_channel", async (channelId) => {
            const channel = await Channel.findOne({ _id: channelId }).select(
              "type"
            );
            if (channel?.type === TypeChannel.LIVECHAT) {
              const vaConfig = await VaConfig.findOne({
                channel: channel?._id,
              });
              channel.vaConfig = vaConfig;
              socket.channelLiveChat = channel;
            }

            if (channel) {
              socket.join(`channel_${channelId}`);
              io.to(socket.id).emit("notification", {
                type: "success",
                message: "Join channel success",
              });
            } else {
              io.to(socket.id).emit(
                "error",
                JSON.stringify(err.CHANNEL_NOT_FOUND)
              );
            }
          });

          socket.on("leave_channel", async (channelId) => {
            console.log("leave________________________");
            const channel = await Channel.exists({ _id: channelId });
            if (channel) {
              socket.leave(`channel_${channelId}`);
              io.to(socket.id).emit("notification", {
                type: "success",
                message: "leave channel success",
              });
            } else
              io.to(socket.id).emit(
                "error",
                JSON.stringify(err.CHANNEL_NOT_FOUND)
              );
          });
          //Kết nối Conversation
          socket.on("join_conversation", async (conversationId) => {
            const conversation = await Conversation.findOne({
              _id: conversationId,
            }).select("_id customer vaConfig");

            if (conversation) {
              socket.conversation = conversation;
              socket.customerId = conversation.customer;
              socket.conversationId = conversation._id;
              socket.join(`conversation_${conversationId}`);
              io.to(socket.id).emit("notification", {
                type: "success",
                message: "join conversation success",
              });
            } else
              io.to(socket.id).emit(
                "error",
                JSON.stringify(err.CONVERSATION_NOT_FOUND)
              );
          });
          socket.on("leave_conversation", async (conversationId) => {
            socket.customerId = null;
            socket.conversationId = null;
            const conversation = await Conversation.exists({
              _id: conversationId,
            });
            if (conversation) {
              socket.leave(`conversation_${conversationId}`);
              io.to(socket.id).emit(
                "notification",
                "Leave conversation success"
              );
            } else
              io.to(socket.id).emit(
                "error",
                JSON.stringify(err.CONVERSATION_NOT_FOUND)
              );
          });
        }

        socket.on(
          "send_message",
          async ({ meta, message: text, time, conversationId, customerId }) => {
            try {
              let message;
              conversationId = conversationId || socket.conversationId;
              customerId = customerId || socket.customerId;
              if (!conversationId)
                throw new Error(err.CONVERSATION_NOT_FOUND.message);
              if (socket.customer) {
                message = await addMessage({
                  from: socket.customer._id,
                  isCustomerSend: true,
                  to: conversationId,
                  conversation: conversationId,
                  message: text,
                  timestamp: new Date() * 1,
                });
                if (
                  socket?.channelLiveChat?.vaConfig?.path?.includes(
                    "https://va-ftech.ftech.ai/rocketchat/send_message"
                  )
                ) {
                  message = message.toObject();
                  const suggest = await sendMessageToSupperAi(customerId, text);
                  if (socket?.channelLiveChat?.vaConfig?.isAutoReply) {
                    setTimeout(async function () {
                      const messageSuggest = await addMessage({
                        from: conversationId,
                        to: customerId,
                        isCustomerSend: false,
                        // user: socket.user._id,
                        conversation: conversationId,
                        message: suggest.text,
                        timestamp: new Date() * 1,
                      });
                      console.log({
                        channel: socket?.channelLiveChat?._id,
                        conversation: socket.conversationId,
                        messageSuggest,
                      });
                      message.text = "test";
                      io.to(`channel_${socket.channelLiveChat?._id}`)
                        .to(`conversation_${socket.conversationId}`)
                        .emit("send_message", messageSuggest);
                    }, 3000);
                  } else {
                    message.vaSuggestion = suggest;
                  }
                }
              } else if (socket.user) {
                // Là user
                message = await addMessage({
                  from: conversationId,
                  to: customerId,
                  user: socket.user._id,
                  conversation: conversationId,
                  message: text,
                  timestamp: new Date() * 1,
                });
              }
              if (!message)
                throw new Error(err.MESSAGE_FIELD_CONV_ID_WRONG.message);
              io.to(`channel_${socket.channelLiveChat?._id}`)
                .to(`conversation_${socket.conversationId}`)
                .emit("send_message", message);
            } catch (error) {
              console.log("___error___", error);
              io.to(socket.id).emit("notification", {
                type: "error",
                message: error?.message,
              });
            }
          }
        );
      } catch (error) {
        io.to(socket.id).emit("notification", {
          type: "error",
          message: error?.message,
        });
      }
    });

    return io;
  } catch (error) {
    console.log(`error`, error);
  }
};
