const { sendMessage } = require("./FacebookAPI");

const timeOut = {};
const questions = {};
const listMessage = [
  "Timi vẫn đang đợi bạn, bạn có muốn trò chuyện thêm với Timi không?",
  "Bạn đừng quên Timi có thể cung cấp thông tin về Ftech cho bạn nhé.",
  "Xin chào, Timi luôn có mặt 24/7, hãy trò chuyện nhiều hơn với Timi nhé.",
  "Timi luôn sẵn sàng trò chuyện với bạn, hãy khám phá thêm về Ftech cùng Timi nhé.",
  "Timi luôn sẵn sàng trò chuyện với bạn, bạn có cần giúp đỡ gì nữa không ạ.",
];

const autoSendMessageFacebook = async (customerId, pageAccessToken) => {
  questions[customerId] = questions[customerId] || 0;
  clearTimeout(timeOut[customerId]);
  timeOut[customerId] = setTimeout(async () => {
    try {
      clearTimeout(timeOut[customerId]);
      await sendMessage(
        customerId,
        pageAccessToken,
        listMessage[questions[customerId]]
      );
      // console.log(questions[customerId] ,"----", listMessage.length);

      questions[customerId] =
        questions[customerId] >= listMessage.length - 1
          ? 0
          : questions[customerId] + 1;
    } catch (error) {
      console.log("___error___",error);;
    }
  }, 3 * 60 * 1000);
};
module.exports = autoSendMessageFacebook;
