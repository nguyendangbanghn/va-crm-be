const request = require("request-promise");
const facebookConfig = require("../../config/facebook.config");

exports.registerWebhookEvent = async (pageId, pageAccessTokens) => {
  const subscribed_fields = [
    "messages",
    "messaging_postbacks",
    "messaging_optins",
    "message_deliveries",
    "message_reads",
    "messaging_payments",
    "messaging_pre_checkouts",
    "messaging_checkout_updates",
    "messaging_account_linking",
    "messaging_referrals",
    "message_echoes",
    "messaging_game_plays",
    "standby",
    "messaging_handovers",
    "messaging_policy_enforcement",
    "message_reactions",
    "inbox_labels",
    "feed",
    "messaging_feedback",
  ].join(",");
  const path = `${pageId}/subscribed_apps?subscribed_fields=${subscribed_fields}&access_token=${pageAccessTokens}`;
  const res = await request({
    method: "POST",
    uri: `${facebookConfig.fb_graph_api_url}/${path}`,
  });
  return JSON.parse(res).success;
};

exports.sendMessage = async (
  recipientId,
  pageAccessToken,
  message,
  attachments
) => {
  try {
    let isSendMessage = true;
    if(!pageAccessToken) {
      return;
    }
    const path = `v12.0/me/messages?access_token=${pageAccessToken}`;
    //đánh giá timi
    if (message === "đánh giá timi") {
      isSendMessage = false;
      await request({
        method: "POST",
        uri: `${facebookConfig.fb_graph_api_url}/${path}`,
        body: {
          messaging_type: "RESPONSE",
          recipient: {
            id: recipientId,
          },
          message: {
            attachment: {
              type: "template",
              payload: {
                template_type: "customer_feedback",
                title: "Đánh giá Timi",
                subtitle: "Vui lòng để lại đánh giá của bạn",
                button_title: "Đánh giá",
                feedback_screens: [
                  {
                    questions: [
                      {
                        id: "va_timi_crm",
                        type: "csat",
                        title: "Vui lòng gửi đánh giá của bạn",
                        score_label: "neg_pos",
                        score_option: "five_stars",
                        follow_up: {
                          type: "free_form",
                          placeholder: "Nhập phản hồi",
                        },
                      },
                    ],
                  },
                ],
                business_privacy: {
                  url: "https://www.ftech.ai",
                },
                expires_in_days: 2,
              },
            },
          },
        },
        json: true,
      });
    }
    //image
    if (attachments?.image) {
      typeof attachments?.image === "string" &&
        (attachments.image = [attachments?.image]);
      await Promise.all(
        attachments?.image?.map((i) =>
          request({
            method: "POST",
            uri: `${facebookConfig.fb_graph_api_url}/${path}`,
            body: {
              messaging_type: "RESPONSE",
              recipient: {
                id: recipientId,
              },
              message: {
                attachment: {
                  type: "image",
                  payload: {
                    is_reusable: true,
                    url: i,
                  },
                },
              },
            },
            json: true,
          })
        )
      );
    }
    //video
    if (attachments?.video) {
      typeof attachments?.video === "string" &&
        (attachments.video = [attachments?.video]);
      await Promise.all(
        attachments?.video?.map((i) =>
          request({
            method: "POST",
            uri: `${facebookConfig.fb_graph_api_url}/${path}`,
            body: {
              messaging_type: "RESPONSE",
              recipient: {
                id: recipientId,
              },
              message: {
                attachment: {
                  type: "video",
                  payload: {
                    is_reusable: true,
                    url: i,
                  },
                },
              },
            },
            json: true,
          })
        )
      );
    }
    //url
    if (attachments?.url) {
      request({
        method: "POST",
        uri: `${facebookConfig.fb_graph_api_url}/${path}`,
        body: {
          messaging_type: "RESPONSE",
          recipient: {
            id: recipientId,
          },
          message: {
            // text: `${attachments?.title}\n${attachments?.url}`,
            attachment: {
              type: "template",
              payload: {
                template_type: "button",
                text: attachments?.title + ": " + attachments?.url,
                buttons: [
                  {
                    type: "web_url",
                    url: attachments?.url,
                    title: "Go url",
                    webview_height_ratio: "full",
                  },
                ],
              },
            },
            //
          },
        },
        json: true,
      });
    }
    //button
    if (0&&attachments?.buttons?.length) {
      isSendMessage = false;
      await request({
        method: "POST",
        uri: `${facebookConfig.fb_graph_api_url}/${path}`,
        body: {
          messaging_type: "RESPONSE",
          recipient: {
            id: recipientId,
          },
          message: {
            // text: message,
            // quick_replies: attachments?.buttons?.map((b) => ({
            //   content_type: "text",
            //   title: b.title,
            //   payload: b.payload,
            // })),

            attachment: {
              type: "template",
              payload: {
                template_type: "button",
                text: message?.includes("utter_faq(_direct_message")
                  ? "Click me"
                  : message,
                buttons: attachments?.buttons?.map((b) => ({
                  type: "postback",
                  title: b.title,
                  payload: b.payload,
                })),
              },
            },
            //
          },
        },
        json: true,
      });



      // typeof attachments?.video === "string" &&
      //   (attachments.video = [attachments?.video]);
      // await Promise.all(
      //   attachments?.video?.map((i) =>
      //     request({
      //       method: "POST",
      //       uri: `${facebookConfig.fb_graph_api_url}/${path}`,
      //       body: {
      //         messaging_type: "RESPONSE",
      //         recipient: {
      //           id: recipientId,
      //         },
      //         message: {
      //           attachment: {
      //             type: "video",
      //             payload: {
      //               is_reusable: true,
      //               url: i,
      //             },
      //           },
      //         },
      //       },
      //       json: true,
      //     })
      //   )
      // );
    }
    const res =
      message &&
      isSendMessage &&
      !message.includes("utter_faq(_direct_message") &&
      (await request({
        method: "POST",
        uri: `${facebookConfig.fb_graph_api_url}/${path}`,
        body: {
          messaging_type: "RESPONSE",
          recipient: {
            id: recipientId,
          },
          message: {
            text: message,
          },
        },
        json: true,
      }));
    return res;
  } catch (error) {
    console.log("___error___:",error);
    return error;
  }
};
exports.actionMessage = async (
  pageAccessToken,
  idCustomer,
  type = "mark_seen" // typing_on typing_off
) => {
  if(!pageAccessToken) return
  const uri = `${facebookConfig.fb_graph_api_url}/v2.6/me/messages?access_token=${pageAccessToken}`;
  const body = {
    recipient: {
      id: idCustomer,
    },
    sender_action: type,
  };
  try {
    const res = await request({
      method: "POST",
      uri,
      json: true,
      headers: {
        "Content-Type": "application/json",
      },
      body,
    });
    return res;
  } catch (error) {
    console.log("___error___:",error);
  }
};
exports.getUserProfile = async (profileId, pageAccessToken) => {
  try {
    const path = `${profileId}?fields=name,profile_pic&access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return JSON.parse(res);
  } catch (error) {
    console.log("___error___:",error);
  }
};

exports.getPageAvatar = async (pageId) => {
  try {
    const path = `${pageId}/picture?width=1920&redirect=0`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return JSON.parse(res);
  } catch (error) {
    console.log("___error___:",error);
  }
};

exports.getListConversations = async (pageId, pageAccessToken) => {
  try {
    const path = `${pageId}/conversations&access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return res;
  } catch (error) {}
};

exports.getListMessage = async (conversationId, pageAccessToken) => {
  try {
    const path = `${conversationId}/messages?access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return res;
  } catch (error) {}
};

exports.getMessage = async (messageId, pageAccessToken) => {
  try {
    const path = `/v9.0/${messageId}?access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return res;
  } catch (error) {}
};

exports.getListConversations = async (pageId, pageAccessToken) => {
  try {
    const path = `${pageId}/conversations&access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return res;
  } catch (error) {}
};

exports.getListMessage = async (conversationId, pageAccessToken) => {
  try {
    const path = `${conversationId}/messages?access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return res;
  } catch (error) {}
};
exports.getListPageFacebook = async (userId, accessToken) => {
  try {
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${userId}/accounts?access_token=${accessToken}`,
    });
    return JSON.parse(res);
  } catch (error) {
    console.log("___error___:",error);
  }
};
exports.getMessage = async (messageId, pageAccessToken) => {
  try {
    const path = `/v9.0/${messageId}?access_token=${pageAccessToken}`;
    const res = await request({
      method: "GET",
      uri: `${facebookConfig.fb_graph_api_url}/${path}`,
    });
    return res;
  } catch (error) {}
};
