const mongoose = require("mongoose");
const { Status } = require("../Enums");
const messageSchema = mongoose.Schema(
  {
    conversation: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "conversation",
      required: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    from: {
      required: true,
      type: String,
      require: true,
    },
    to: {
      required: true,
      type: String,
      require: true,
    },
    message: {
      //text
      type: String,
    },
    timestamp: {
      type: Number,
    },
    attachments: {
      type: Array,
    },
    mid: {
      type: String,
    },
    reactions: {
      type: [
        {
          sender: String,
          emoji: String,
          action: String,
          timestamp: String,
        },
      ],
    },
    entities: [
      {
        keyword: String,
        entity: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "entity",
        },
      },
    ],
    intents: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "intent",
      },
    ],
    response: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "message",
    },
    isCustomerSend: {
      type: Boolean,
    },
  },
  { timestamps: true }
);
const Message = mongoose.model("Message", messageSchema);
module.exports = Message;
