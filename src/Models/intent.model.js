const mongoose = require("mongoose");
const intentSchema = mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
    },
    define: {
      type: String,
    },
    example: {
      type: String,
    },
    // messages: [
    //   {
    //     message: {
    //       type: mongoose.Schema.Types.ObjectId,
    //       ref: "message",
    //     },
    //   },
    // ],
  },
  { timestamps: true }
);
const Intent = mongoose.model("Intent", intentSchema);
module.exports = Intent;
