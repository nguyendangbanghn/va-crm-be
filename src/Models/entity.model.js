const mongoose = require("mongoose");
const entitySchema = mongoose.Schema(
  {
    name: {
      type: String,
      unique: true,
    },
    define: {
      type: String,
    },
    example: {
      type: String,
    },
    // keywords: [
    //   {
    //     keyword: String,
    //     message: {
    //       type: mongoose.Schema.Types.ObjectId,
    //       ref: "message",
    //     },
    //   },
    // ],
  },
  { timestamps: true }
);
const Entity = mongoose.model("Entity", entitySchema);
module.exports = Entity;
