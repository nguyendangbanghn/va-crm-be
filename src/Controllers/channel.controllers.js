const { TypeChannel } = require("../Enums");
const Channel = require("../Models/channel.model");
const Project = require("../Models/project.model");
const err = require("../Errors/index");
const { errorHandler, successHandler } = require("../Utils/ResponseHandler");
const conn = require("../db");
const ObjectID = require("mongodb").ObjectID;
module.exports.getAccessToken = async (channelId) => {
  try {
    const { accessToken } = await Channel.findOne({ id: channelId }).select(
      "accessToken -_id"
    );
    return accessToken;
  } catch (error) {
    console.log("___error___", error);
  }
};
module.exports.getChannelDetail = async (channelId) => {
  try {
    return await Channel.findOne({ _id: channelId });
  } catch (error) {
    console.log("___error___", error);
  }
};
module.exports.getChannelLiveChatByProjectId = async (req, res) => {
  try {
    const projectId = req?.params?.id;
    const project = await Project.findOne({ _id: projectId }).populate(
      "channels",
      "_id type name projects bigLogo smallLogo vaConfig description"
    );
    let liveChat = project?.channels
      ?.find((c) => c.type === TypeChannel.LIVECHAT)
      ?.toObject();
    if (!liveChat) return errorHandler(res, err.CHANNEL_NOT_FOUND);
    liveChat.projectName = project.name;
    return successHandler(res, liveChat);
  } catch (error) {
    console.log("___error___", error);
    errorHandler(res, error);
  }
};
module.exports.configLiveChat = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const { body } = req;
      const project = await Project.findOne({ _id: body.projectId }).populate(
        "channels"
      );
      if (!project) return errorHandler(res, err.PROJECT_NOT_FOUND);
      let channel = new Channel({
        ...body,
        type: TypeChannel.LIVECHAT,
      });
      const channelExist = project?.channels?.find(
        (c) => c.type === TypeChannel.LIVECHAT
      );
      if (channelExist) {
        channel = await Channel.findOneAndUpdate({ _id: channelExist }, body, {
          session,
          new: true,
        });
      } else {
        channel._id = ObjectID();
        channel.projects = [project._id];
        await channel.save({ new: true, session });
        project.channels = [...project.channels, channel._id];
        await project.save({ new: true, session });
      }
      return successHandler(res, { channel });
    });
    session.endSession();
  } catch (error) {
    console.log("___error___", error);
    errorHandler(res, error);
  }
};
