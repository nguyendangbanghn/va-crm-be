const request = require("request-promise");
const stringeexConfigs = require("../../config/stringeex.config");
const { successHandler, errorHandler } = require("../Utils/ResponseHandler");

module.exports.getToken = async (req, res) => {
  try {
    const { portals } = await request({
      method: "POST",
      uri: `${stringeexConfigs.stringeex_url}/account`,
      body: {
        email: stringeexConfigs.email,
        password: stringeexConfigs.password,
      },
      json: true,
    });
    successHandler(res, { auth_token: portals[0].auth_token }, 200);
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
