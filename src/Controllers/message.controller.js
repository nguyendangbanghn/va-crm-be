const Message = require("../Models/message.model");

const err = require("../Errors/index");
const conn = require("../db/index");
const { successHandler, errorHandler } = require("../Utils/ResponseHandler");
const msgConfig = require("../../config/message.config");
const { markReadMessage, actionMessage } = require("../Utils/FacebookAPI");
const Channel = require("../Models/channel.model");
const Conversation = require("../Models/conversation.model");
const { Parser } = require("json2csv");
const Intent = require("../Models/intent.model");
const Entity = require("../Models/entity.model");
const {
  mongo: { ObjectId },
} = require("mongoose");
module.exports.create = async (req, res) => {
  try {
    const { body } = req;
    const message = new Message(body);
    await message.save();
    successHandler(res, message);
  } catch (error) {
    debugger;
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.addMessage = async (message) => {
  try {
    let newMessage = null;
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      newMessage = new Message(message);
      await newMessage.save({ new: true, session });
      const conversationExist = await Conversation.findOne({
        _id: newMessage.conversation,
      });
      if (!conversationExist)
        throw new Error(err.CONVERSATION_NOT_FOUND.message);
      conversationExist?.messages.push(newMessage._id);
      await conversationExist.save({ new: true, session });
    });
    session.endSession();
    return newMessage;
  } catch (error) {
    console.log("___error___", error);
  }
};

module.exports.getMessageByConversation = async (req, res) => {
  try {
    let { conversationId, page = 1, limit = 10 } = req?.query;
    limit = limit * 1;
    page = page * 1;
    const skip = (page - 1) * limit;
    const messages = await Message.find({ conversation: conversationId })
      .select("attachments from to message timestamp")
      .sort({ timestamp: -1 })
      .limit(limit)
      .skip(skip);
    const total = await Message.count({ conversation: conversationId });
    successHandler(res, {
      messages,
      paginate: {
        page,
        limit,
        total,
        totalPage: Math.ceil(total / limit),
      },
    });
  } catch (error) {
    debugger;
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.actionMessageHandler = async (req, res) => {
  try {
    const { conversationId, type } = req.body;
    const conversation = await Conversation.findOne({
      _id: conversationId,
    });
    if (!conversation) return errorHandler(res, err.CONVERSATION_NOT_FOUND);
    const channel = await Channel.findOne({ _id: conversation.channel }).select(
      "accessToken -_id"
    );
    if (!channel) return errorHandler(res, err.CHANNEL_NOT_FOUND);
    const result = await actionMessage(
      channel.accessToken,
      conversation.customer,
      type
    );
    if (!result) return errorHandler(res, "Error");
    conversation.seen.channel = new Date() * 1;
    await conversation.save();
    return successHandler(res, { result });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};

module.exports.exportCsv = async (req, res) => {
  try {
    const { channelId, type } = req.query;
    let messages = [];
    if (channelId) {
      const channel = await Channel.findById(channelId).select("conversations");
      if (!channel) return errorHandler(res, err.CHANNEL_NOT_FOUND);

      messages = await Message.find({
        conversation: { $in: channel.conversations },
        ...(type === "nlu" ? { isCustomerSend: true } : {}),
      })
        .populate("entities.entity", null, Entity)
        .populate("intents", null, Intent);
    } else {
      messages = await Message.find()
        .populate("entities.entity", null, Entity)
        .populate("intents", null, Intent);
    }
    res.attachment("exportJSON.json");
    res.set({ "content-type": "application/json; charset=utf-8" });
    res.status(200).send(JSON.stringify(messages));
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};

module.exports.updateEntity = async (req, res) => {
  try {
    const id = req.params.id;
    const entities = req?.body?.entities;
    let entitiesExists = await Entity.find({
      _id: { $in: entities?.map((e) => e.entity) },
    }).select("_id");
    entitiesExists = entitiesExists.map((e) => e?._id?.toString());
    const entitiesNotExists = entities.filter((e) => {
      return !entitiesExists.includes(e.entity);
    });
    if (entitiesNotExists?.length)
      return errorHandler(res, {
        ...err.ENTITY_NOT_FOUND,
        data: `Không tìm thấy entity ${JSON.stringify(
          entitiesNotExists?.map((e) => e.entity)
        )} trên hệ thống`,
      });
    const message = await Message.findOneAndUpdate(
      { _id: id },
      { entities },
      { new: true }
    );
    return successHandler(res, { message });
  } catch (error) {
    return errorHandler(res, error);
  }
};

module.exports.updateIntent = async (req, res) => {
  try {
    const id = req.params.id;
    const intents = req?.body?.intents;
    let intentExists = await Intent.find({
      _id: { $in: intents },
    }).select("_id");

    intentExists = intentExists.map((e) => e?._id?.toString());
    const intentNotExists = intents.filter((e) => {
      return !intentExists.includes(e);
    });
    if (intentNotExists?.length)
      return errorHandler(res, {
        ...err.INTENT_NOT_FOUND,
        data: `Không tìm thấy intent ${JSON.stringify(
          intentNotExists
        )} trên hệ thống`,
      });
    const message = await Message.findOneAndUpdate(
      { _id: id },
      { intents },
      { new: true }
    );
    return successHandler(res, { message });
  } catch (error) {
    return errorHandler(res, error);
  }
};

module.exports.searchMessages = async (req, res) => {
  try {
    let { channelId, ...search } = req.body;

    const channel = await Channel.findOne({ _id: channelId }).select(
      "conversations"
    );

    if (!channel) return errorHandler(res, err.CHANNEL_NOT_FOUND);

    if (!channel?.conversations)
      return errorHandler(res, err.MESSAGE_NOT_FOUND);

    if (search?.conversationId) {
      const listConversation = search?.conversationId?.map((c) =>
        channel?.conversations?.includes(c)
      );

      if (!listConversation) return errorHandler(res, err.MESSAGE_NOT_FOUND);
      search.conversation = listConversation;
    } else {
      search.conversation = channel?.conversations;
    }

    const q = {};
    for (const i in search) {
      q[i === "entities" ? "entities.entity" : i] = {
        $in: search[i]?.map((v) => new ObjectId(v)),
      };
    }
    let messages = await Message.find(q)
      .populate("entities.entity", null, Entity)
      .populate("intents", null, Intent);
    if (!messages) return errorHandler(res, err.MESSAGE_NOT_FOUND);

    messages = messages
      ?.map((m, i) => {
        const nextMessage = messages?.[i + 1];
        if (m.isCustomerSend && nextMessage?.isCustomerSend === false) {
          m = m.toObject();
          m.response = nextMessage.toObject();
        }
        if (m.isCustomerSend) return m;
      })
      .filter((m) => m);

    return successHandler(res, { messages });
  } catch (error) {
    return errorHandler(res, error);
  }
};
