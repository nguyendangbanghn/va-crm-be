const Message = require("../Models/message.model");
const err = require("../Errors/index");
const conn = require("../db/index");
const { successHandler, errorHandler } = require("../Utils/ResponseHandler");
const Intent = require("../Models/intent.model");

module.exports.findAll = async (req, res) => {
  try {
    let { page = 1, limit = 10 } = req?.query;
    limit = limit * 1;
    page = page * 1;
    const skip = (page - 1) * limit;
    const messages = await Intent.find()
      .sort({ updateAt: -1 })
      .limit(limit)
      .skip(skip);
    const total = await Intent.count();
    successHandler(res, {
      messages,
      paginate: {
        page,
        limit,
        total,
        totalPage: Math.ceil(total / limit),
      },
    });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};

module.exports.create = async (req, res) => {
  try {
    const intent = new Intent(req.body);
    await intent.save({ new: true });
    successHandler(res, { intent });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.update = async (req, res) => {
  try {
    const id = req?.params?.id;
    const intent = await Intent.findOneAndUpdate({ _id: id }, req.body, {
      new: true,
    });
    if (!intent) throw new Error(err.INTENT_NOT_FOUND.message);
    successHandler(res, { intent });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.delete = async (req, res) => {
  try {
    const id = req?.params?.id;
    const intent = await Intent.deleteOne({ _id: id });
    console.log(intent);
    // if (!intent) throw new Error(err.INTENT_NOT_FOUND.message);
    successHandler(res, { intent });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
// module.exports.g = async (message) => {
//   try {
//     let newMessage = null;
//     const session = await conn.startSession();
//     await session.withTransaction(async () => {
//       newMessage = new Message(message);
//       await newMessage.save({ new: true, session });
//       const conversationExist = await Conversation.findOne({
//         _id: newMessage.conversation,
//       });
//       if (!conversationExist)
//         throw new Error(err.CONVERSATION_NOT_FOUND.message);
//       conversationExist?.messages.push(newMessage._id);
//       await conversationExist.save({ new: true, session });
//     });
//     session.endSession();
//     return newMessage;
//   } catch (error) {
//     console.log("___error___", error);
//   }
// };
