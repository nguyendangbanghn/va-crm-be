const VaConfig = require("../Models/vaConfig.model");
const conn = require("../db/index");

const err = require("../Errors/index");
const { successHandler, errorHandler } = require("../Utils/ResponseHandler");
const Channel = require("../Models/channel.model");
const { sendMessageToAi } = require("../Utils/AiUtils");

module.exports.createVaConfig = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const { body } = req;
      if (
        !body?.path?.includes(
          "https://va-ftech.ftech.ai/rocketchat/send_message"
        )
      ) {
        const testVaMessage = await sendMessageToAi(
          "va-crm-test",
          body,
          "test_message"
        );
        if (testVaMessage?.error) {
          return res.status(200).json({
            success: err.UNKNOWN_ERROR.success,
            data: null,
            message: `VA config error: ${testVaMessage?.message}`,
            code: 1100,
            messageCode: "SEND_VA_ERROR",
          });
        }
      }
      let vaConfig;
      vaConfig = await VaConfig.findOneAndUpdate(
        { channel: body.channel },
        body,
        { session, new: true }
      );
      if (!vaConfig) {
        vaConfig = new VaConfig(body);
        await vaConfig.save({ session });
      }
      const channel = await Channel.findOneAndUpdate(
        { _id: body.channel },
        { vaConfig: vaConfig._id },
        { session }
      ); //.populate("vaConfig", "-password -channel -__v -createdAt -updatedAt")
      if (!channel) return errorHandler(res, err.CHANNEL_NOT_FOUND);
      successHandler(res, vaConfig, 201);
    });
    session.endSession();
  } catch (error) {
    debugger;
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.updateAutoRepLy = async (req, res) => {
  try {
    const idVa = req.params.id;
    const va = await VaConfig.findOneAndUpdate(
      { _id: idVa },
      { isAutoReply: req.body.isAutoReply },
      { new: true, useFindAndModify: false }
    );
    if (!va) return errorHandler(res, err.VA_CONFIG_NOT_FOUND);
    successHandler(res, va, 201);
  } catch (error) {
    return errorHandler(res, error);
  }
};
module.exports.getVaConfig = async (req, res) => {
  try {
    const { body } = req;
    const idChannel = req?.params?.id;
    const result = await VaConfig.findOne({
      channel: body.channel || idChannel,
    });
    successHandler(res, result, 200);
  } catch (error) {
    debugger;
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
