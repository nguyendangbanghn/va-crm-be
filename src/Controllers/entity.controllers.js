const Message = require("../Models/message.model");
const err = require("../Errors/index");
const conn = require("../db/index");
const { successHandler, errorHandler } = require("../Utils/ResponseHandler");
const Entity = require("../Models/entity.model");

module.exports.findAll = async (req, res) => {
  try {
    let { page = 1, limit = 10 } = req?.query;
    limit = limit * 1;
    page = page * 1;
    const skip = (page - 1) * limit;
    const entities = await Entity.find()
      .sort({ updateAt: -1 })
      .limit(limit)
      .skip(skip);
    const total = await Entity.count();
    successHandler(res, {
      messages: entities,
      paginate: {
        page,
        limit,
        total,
        totalPage: Math.ceil(total / limit),
      },
    });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};

module.exports.create = async (req, res) => {
  try {
    const entity = new Entity(req.body);
    await entity.save({ new: true });
    successHandler(res, { entity });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.update = async (req, res) => {
  try {
    const id = req?.params?.id;
    const entity = await Entity.findOneAndUpdate({ _id: id }, req.body, {
      new: true,
    });
    if (!entity) return errorHandler(res, err.ENTITY_NOT_FOUND);
    successHandler(res, { entity });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
module.exports.delete = async (req, res) => {
  try {
    const id = req?.params?.id;
    const entity = await Entity.deleteOne({ _id: id });
    if (!entity?.deletedCount) return errorHandler(res, err.ENTITY_NOT_FOUND);
    successHandler(res, { entity });
  } catch (error) {
    console.log("___error___", error);
    return errorHandler(res, error);
  }
};
// module.exports.g = async (message) => {
//   try {
//     let newMessage = null;
//     const session = await conn.startSession();
//     await session.withTransaction(async () => {
//       newMessage = new Message(message);
//       await newMessage.save({ new: true, session });
//       const conversationExist = await Conversation.findOne({
//         _id: newMessage.conversation,
//       });
//       if (!conversationExist)
//         throw new Error(err.CONVERSATION_NOT_FOUND.message);
//       conversationExist?.messages.push(newMessage._id);
//       await conversationExist.save({ new: true, session });
//     });
//     session.endSession();
//     return newMessage;
//   } catch (error) {
//     console.log("___error___", error);
//   }
// };
