const { TypeChannel } = require("../Enums");
const Channel = require("../Models/channel.model");
const Conversation = require("../Models/conversation.model");
const Customer = require("../Models/customer.model");
const Message = require("../Models/message.model");
const {
  getUserProfile,
  sendMessage: sendMessageFacebook,
  actionMessage,
} = require("../Utils/FacebookAPI");
const {
  sendMessageToAi,
  getChannelConfig,
  sendMessageToSupperAi,
} = require("../Utils/AiUtils");
const { getAccessToken, getChannelDetail } = require("./channel.controllers");
const { findOneOrCreateConversation } = require("./conversation.controllers");
const { customerExist, createCustomer } = require("./customer.controllers");
const autoSendMessageFacebook = require("../Utils/autoSendMessageFacebook");

module.exports.registerWebhook = async (req, res) => {
  try {
    // Your verify token. Should be a random string.
    let VERIFY_TOKEN = "ftechai";

    // Parse the query params
    let mode = req.query["hub.mode"];
    let token = req.query["hub.verify_token"];
    let challenge = req.query["hub.challenge"];
    // Checks if a token and mode is in the query string of the request
    if (mode && token) {
      // Checks the mode and token sent is correct
      if (mode === "subscribe" && token === VERIFY_TOKEN) {
        // Responds with the challenge token from the request
        console.log("WEBHOOK_VERIFIED");
        res.status(200).send(challenge);
      } else {
        // Responds with '403 Forbidden' if verify tokens do not match
        res.sendStatus(403);
      }
    }
  } catch (error) {
    res.sendStatus(403);
  }
};

module.exports.recieveDataWebhookSocket = async (req, res, io) => {
  try {
    let body = req.body;
    let newConversation = false;
    if (body.object === "page") {
      const data = body?.entry?.[0];
      const pageId = data?.id;
      const channel = await getChannelDetail(pageId);
      const message = data.messaging?.[0];
      const textMessage =
        message?.message?.text || data.messaging?.[0]?.postback?.title;
      const customerId =
        message?.sender?.id === pageId
          ? message?.recipient?.id
          : message?.sender?.id;
      const feedback =
        data?.messaging?.[0]?.messaging_feedback?.feedback_screens?.[0]
          ?.questions?.va_timi_crm;
      const reaction = data?.messaging?.[0]?.reaction;
      const read = data?.messaging?.[0]?.read;
      let conversation = await Conversation.findOne({
        channel: pageId,
        customer: customerId,
      });
      if (read) {
        if (conversation) {
          conversation.seen[
            conversation.channel === data?.messaging?.[0]?.sender?.id
              ? "chanel"
              : "customer"
          ] = data?.messaging?.[0]?.read?.watermark;
          await conversation.save({ new: true });
          !!conversation?._id &&
            io
              .to(`conversation_${conversation?._id}`)
              .emit("dataConversation", {
                conversation,
              });
        }
      } else if (reaction) {
        const sender = message?.sender?.id;
        const m = await Message.findOne({ mid: reaction.mid });
        if (!m) return;
        const newReact = {
          sender,
          emoji: reaction?.emoji,
          action: reaction?.action,
          timestamp: message.timestamp,
        };
        const _m = m?.reactions?.find((o, i) => {
          if (o.sender === sender) {
            m.reactions[i] = newReact;
            return true;
          }
        });
        if (!_m) m.reactions.push(newReact);
        m.save();
      } else if (feedback) {
        sendMessageToSupperAi(
          customerId,
          `/rating_conversation{'value': "${feedback?.payload}"}`,
          feedback?.follow_up?.payload
        );
        sendMessageFacebook(
          customerId,
          channel?.accessToken,
          `Cảm ơn bạn đã gửi đánh giá đến Timi !!!`
        );
      } else if (
        data.messaging?.[0]?.message ||
        data.messaging?.[0]?.postback
      ) {
        if ((await customerExist(customerId)) <= 0 && channel?.accessToken) {
          const customerInfo = await getUserProfile(
            customerId,
            channel?.accessToken
          );
          createCustomer(
            new Customer({
              type: TypeChannel.FACEBOOK,
              _id: customerInfo.id,
              name: customerInfo.name,
              avatar: customerInfo.profile_pic,
              gender: customerInfo.gender?.toUpperCase(),
              timezone: customerInfo.timezone,
              locale: customerInfo.locale,
              channel: channel._id,
            })
          );
        }
        // const conversation = await findOneOrCreateConversation(pageId, customerId);

        if (!conversation) {
          newConversation = true;
          const channel = await getChannelDetail(pageId);
          conversation = new Conversation({
            // project: channel?.project,
            channel: pageId,
            customer: customerId,
          });
          await conversation.save();
          channel.conversations.push(conversation._id);
          await channel.save();
        }
        if (conversation) {
          const newMessage = new Message({
            conversation: conversation._id,
            from: message?.sender?.id,
            to: message?.recipient?.id,
            isCustomerSend: message?.sender?.id === customerId,
            message: textMessage,
            timestamp: message?.timestamp,
            attachments: message?.message?.attachments,
            mid: message?.message?.mid,
          });
          await newMessage.save();
          conversation.messages.push(newMessage._id);
          await conversation.save();
          const config = await getChannelConfig(pageId);
          let vaSuggestion;
          if (config?.path && message?.sender?.id != pageId) {
            vaSuggestion = await sendMessageToAi(
              customerId,
              config,
              textMessage
            );
            if (vaSuggestion?.error) {
              io.to(`conversation_${conversation._id}`).emit(
                "error",
                JSON.stringify({
                  status: 200,
                  success: false,
                  data: null,
                  message: `VA config error: ${vaSuggestion?.message}`,
                  code: 1100,
                  messageCode: "SEND_VA_ERROR",
                })
              );
              vaSuggestion = null;
            }
          }
          if (newConversation) {
            const customerDetail = await Customer.findOne({ _id: customerId });
            const channelDetail = await Channel.findOne({ _id: pageId }).select(
              "-accessToken -conversations"
            );
            io.to(`channel_${channel._id}`).emit("new_conversation", {
              ...conversation.toObject(),
              customerDetail,
              channelDetail,
              lastMessageDetail: { ...newMessage.toObject(), vaSuggestion },
            });
          }

          // https://test.vago.ftech.ai/core/webhooks/crmrest/webhook
          // pass BsnaEEVZ2RTsPgtP
          //autoReply
          // console.log("rep",config?.isAutoReply,vaSuggestion?.[0]?.recipient_id===customerId,!!channel?.accessToken,!!vaSuggestion?.[0].text);
          // (config?.isAutoReply&&vaSuggestion?.[0]?.recipient_id===customerId&&!!channel?.accessToken&&!!vaSuggestion?.[0].text)&&sendMessageFacebook(customerId,channel?.accessToken,vaSuggestion?.[0].text)
          if (message?.sender?.id === customerId && config?.isAutoReply) {
            autoSendMessageFacebook(customerId, channel?.accessToken);

            // if(pageId==="108802470501867"&& message?.sender?.id===customerId&&config?.isAutoReply){
            const handlerAutoReply = async () => {
              const res = await sendMessageToSupperAi(
                customerId,
                data.messaging?.[0]?.postback?.payload || textMessage
              );
              // console.log("res_________________",res);
              sendMessageFacebook(
                customerId,
                channel?.accessToken,
                res?.text,
                res?._meta
              );
            };
            if (
              textMessage?.toLowerCase()?.includes("đánh giá timi") ||
              textMessage?.toLowerCase()?.includes("danh gia timi")
            ) {
              sendMessageFacebook(
                customerId,
                channel?.accessToken,
                "đánh giá timi"
              );
            } else {
              actionMessage(channel?.accessToken, customerId, "typing_on");
              setTimeout(handlerAutoReply, 3000);
            }
          }
          !!channel?._id &&
            io.to(`channel_${channel?._id}`).emit("new_channel_message", {
              ...newMessage.toObject(),
              vaSuggestion,
            });
          !!conversation?._id &&
            io
              .to(`conversation_${conversation?._id}`)
              .emit("new_conversation_message", {
                ...newMessage.toObject(),
                vaSuggestion,
              });
        }
      } else {
        // console.log(data.messaging?.[0]);
      }
      res.status(200).send("EVENT_RECEIVED");
    } else {
      // Returns a '404 Not Found' if event is not from a page subscription
      res.sendStatus(404);
    }
  } catch (error) {
    console.log("___error___", error);
    res.sendStatus(404);
  }
};
