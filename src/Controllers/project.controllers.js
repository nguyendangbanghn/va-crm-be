const User = require("../Models/user.model");
const Project = require("../Models/project.model");
const { RoleProject, Status, TypeChannel } = require("../Enums");
const err = require("../Errors/index");
const conn = require("../db/index");
const { successHandler, errorHandler } = require("../Utils/ResponseHandler");
const passport = require("passport");
const Channel = require("../Models/channel.model");
const {
  registerWebhookEvent,
  getMessage,
  getPageAvatar,
} = require("../Utils/FacebookAPI");
const ObjectID = require("mongodb").ObjectID;
exports.create = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const idChannel = ObjectID();
      let project = new Project({
        ...req.body,
      });
      project.users = [
        {
          userId: req.user._id,
          role: RoleProject.ADMIN,
          username: req.user.username,
          email: req.user.email,
        },
      ];
      project.channels = [idChannel];
      await project.save({ new: true, session });
      const channel = new Channel({
        _id: idChannel,
        type: TypeChannel.LIVECHAT,
        projects: [project._id],
      });
      await channel.save({ new: true, session });
      console.log(channel._id);

      const user = await User.findOneAndUpdate(
        { _id: req.user._id, status: Status.ACTIVE },
        {
          $addToSet: {
            projects: [
              {
                role: RoleProject.ADMIN,
                projectId: project._id,
                projectName: project.name,
              },
            ],
          },
        },
        { new: true, useFindAndModify: false, session }
      );
      successHandler(res, { project, user }, 200);
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    if (error.code === 11000)
      return errorHandler(res, err.PROJECT_DUPLICATED, error);
    return errorHandler(res, error);
  }
};

exports.delete = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const projectId = req.params.id;
      const project = await Project.findOneAndUpdate(
        {
          _id: projectId,
          status: Status.ACTIVE,
        },
        {
          status: Status.DELETE,
          users: [],
          channels: [],
        },
        { session }
      );
      if (!project) return errorHandler(res, err.PROJECT_NOT_FOUND);
      const users = await Promise.all(
        project.users.map((u) =>
          User.findOneAndUpdate(
            { _id: u.userId, status: Status.ACTIVE },
            {
              $pull: {
                projects: {
                  projectId: projectId,
                },
              },
            },
            { new: true, useFindAndModify: false, session }
          )
        )
      );
      await Channel.updateMany(
        { _id: { $in: project.channels } },
        { $pull: { projects: projectId } },
        { session }
      );
      // await Channel.deleteMany({ _id: { $in: project.channels } });
      successHandler(res, { project, users }, 200);
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
// exports.create = async (req, res) => {
//     try {
//         const session = await conn.startSession();
//         await session.withTransaction(async () => {
//             let project = new Project({
//                 ...req.body,
//             });
//             await project.save({ session });
//             const users = await Promise.all(
//                 project.users.map(u => {
//                     return User.findByIdAndUpdate(u.userId, {
//                         $addToSet: {
//                             projects: [{
//                                 role: u.role,
//                                 projectId: project._id,
//                                 projectName: project.name,
//                             }]
//                         }
//                     }, { new: true, useFindAndModify: false, session })
//                 })
//             )
//             successHandler(res, { project }, 200)
//         })
//         session.endSession();
//     } catch (error) {
//         console.log("___error___",error);;
//         if (error.code === 11000)
//             return errorHandler(res, err.PROJECT_DUPLICATED, error)
//         return errorHandler(res, error)
//     }
// };
exports.detail = async (req, res) => {
  try {
    const projectId = req.params.id;
    const project = await Project.findOne({
      _id: projectId,
      status: Status.ACTIVE,
    }).populate({
      path: "channels",
      select: "-accessToken -createdAt -status -updatedAt -__v",
      populate: {
        path: "vaConfig",
        select: " -channel -createdAt -updatedAt -__v",
      },
    });
    if (!project?.channels?.vaConfig) project.channels.vaConfig = null;
    if (!project) return errorHandler(res, err.PROJECT_NOT_FOUND.messageCode);
    
    successHandler(res, { project }, 200);
  } catch (error) {
    console.log("___error___",error);;
    return errorHandler(res, error);
  }
};
exports.addUsers = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const projectId = req.params.id;
      const project = await Project.findOneAndUpdate(
        { _id: projectId, status: Status.ACTIVE },
        {
          $addToSet: {
            users: { $each: req?.body?.users },
          },
        },
        { new: true, useFindAndModify: false, session }
      );
      if (!project) return errorHandler(res, err.PROJECT_NOT_FOUND);
      const users = await Promise.all(
        req?.body?.users.map((u) =>
          User.findOneAndUpdate(
            { _id: u.userId, status: Status.ACTIVE },
            {
              $addToSet: {
                projects: [
                  {
                    role: u.role,
                    projectId: project._id,
                    projectName: project.name,
                  },
                ],
              },
            },
            { new: true, useFindAndModify: false, session }
          )
        )
      );
      successHandler(res, { project, users }, 200);
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
exports.removeUsers = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const projectId = req.params.id;
      const userIds = req.body.users;
      const project = await Project.findOneAndUpdate(
        { _id: projectId, status: Status.ACTIVE },
        {
          $pull: {
            users: {
              userId: { $in: userIds },
            },
          },
        },
        { new: true, useFindAndModify: false, session }
      );
      const users = await Promise.all(
        userIds.map((userId) =>
          User.findOneAndUpdate(
            { _id: userId, status: Status.ACTIVE },
            {
              $pull: {
                projects: {
                  projectId: projectId,
                },
              },
            },
            { new: true, useFindAndModify: false, session }
          )
        )
      );
      successHandler(res, { project, users }, 200);
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
exports.updateRoleUsers = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const projectId = req.params.id;
      const set = {};
      const arrayFilters = [];
      req.body?.map((u) => {
        set[`users.$[e${u.userId}].role`] = u.role;
        arrayFilters.push({
          [`e${u.userId}.userId`]: u.userId,
        });
      });
      const project = await Project.findOneAndUpdate(
        { _id: projectId, status: Status.ACTIVE },
        {
          $set: set,
        },
        {
          arrayFilters,
          new: true,
          useFindAndModify: false,
          session,
        }
      );
      const users = await Promise.all(
        req.body.map((u) =>
          User.findOneAndUpdate(
            { _id: u.userId, status: Status.ACTIVE },
            {
              $set: { "projects.$[e].role": u.role },
            },
            {
              arrayFilters: [{ "e.projectId": project._id }],
              new: true,
              useFindAndModify: false,
              session,
            }
          )
        )
      );
      successHandler(res, { project, users }, 200);
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
exports.updateName = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const project = await Project.findOneAndUpdate(
        { _id: req.params.id, status: Status.ACTIVE },
        { name: req.body.name },
        { new: true, useFindAndModify: false, session }
      );
      if (!project) return errorHandler(res, err.PROJECT_NOT_FOUND);
      const users = await User.updateMany(
        { _id: { $in: project?.users?.map((u) => u.userId) } },
        {
          $set: {
            "projects.$[e].projectName": project.name,
          },
        },
        {
          arrayFilters: [{ "e.projectId": project._id }],
          new: true,
          useFindAndModify: false,
          session,
        }
      );
      successHandler(res, { project }, 200);
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
exports.list = async (req, res) => {
  try {
    const projects = await Project.find({
      "users.userId": req.user._id,
      status: Status.ACTIVE,
    })
      .populate("channels")
      .limit(1000)
      .skip(0);
    successHandler(res, { projects }, 200);
  } catch (error) {
    console.log("___error___",error);;
    errorHandler(res, error);
  }
};
// exports.addChannels = async (projectId, configChannel) => {
//     return await Project.findOneAndUpdate({ _id: projectId, status: Status.ACTIVE }, {
//         $addToSet: {
//             channels: { $each: configChannel }
//         }
//     }, { new: true, useFindAndModify: false });
// }
exports.connectFacebook = async (req, res, next) => {
  try {
    passport
      .authenticate("facebook", {
        state: JSON.stringify({
          projectId: req.params.id,
          referer: req?.headers?.referer,
        }),
        scope: [
          "email",
          "pages_manage_metadata",
          "public_profile",
          "pages_manage_engagement",
          "pages_show_list",
          "read_page_mailboxes",
          "pages_messaging",
          "pages_messaging_subscriptions",
          "pages_read_user_content",
          "pages_read_engagement",
          "read_insights",
        ],
      })
      .call(this, req, res, next);
  } catch (error) {
    console.log("___error___",error);;
    res.redirect(
      req?.headers?.referer +
        "project/" +
        req.params.id +
        "?success=false&message=" +
        error.message
    );
  }
};
exports.connectFacebookCallback = async (req, res, next) => {
  try {
    passport
      .authenticate("facebook", async function (_err, data, info) {
        const { projectId, referer } = JSON.parse(req?.query?.state);
        try {
          if (_err || !data) {
            if (!data)
              return res.redirect(
                referer + "project/" + projectId + "?success=false"
              );
            throw _err;
          } else {
            const session = await conn.startSession();
            await session.withTransaction(async () => {
              const project = await Project.findOne({
                _id: projectId,
                status: Status.ACTIVE,
              });
              if (!project)
                return res.redirect(
                  referer +
                    "project/" +
                    projectId +
                    "?success=false&message=" +
                    err.PROJECT_NOT_FOUND.message +
                    "&messageCode=" +
                    err.PROJECT_NOT_FOUND.messageCode
                );
              const channelsOff = await Channel.updateMany(
                {
                  userFacebook: data.userFacebook,
                  _id: { $nin: data?.page?.map((c) => c.id) },
                },
                { isEnableMessage: false },
                { session }
              );
              const channels = await Promise.all(
                data?.page?.map(async (c) => {
                  const avt = await getPageAvatar(c.id);
                  let chanel = await Channel.findOne({ _id: c.id });
                  const isEnableMessage = await registerWebhookEvent(
                    c.id,
                    c.access_token
                  );
                  const _page = chanel || new Channel({ _id: c.id });
                  _page.type = TypeChannel.FACEBOOK;
                  _page.name = c.name;
                  _page.accessToken = c.access_token;
                  _page.isEnableMessage = isEnableMessage;
                  _page.avatar = avt?.data?.url;
                  _page.projects.indexOf(projectId) === -1 &&
                    _page.projects.push(projectId);
                  _page.userFacebook = data.userFacebook;
                  await _page.save({ new: true, session });
                  project.channels.indexOf(_page._id) === -1 &&
                    project.channels.push(_page.id);
                  return _page;
                })
              );
              await project.save({ session });
              res.redirect(referer + "project/" + projectId + "?success=true");
            });
            session.endSession();
          }
        } catch (error) {
          console.log("___error___",error);;
          if (error.code === 11000)
            res.redirect(
              referer +
                "project/" +
                projectId +
                "?success=false&message=" +
                err.CHANNEL_DUPLICATED.message +
                "&messageCode=" +
                err.CHANNEL_DUPLICATED.messageCode
            );
          res.redirect(
            referer +
              "project/" +
              projectId +
              "?success=false&message=" +
              error.message
          );
        }
      })
      .call(this, req, res, next);
  } catch (error) {
    errorHandler(res, error);
  }
};
exports.removeChannels = async (req, res) => {
  try {
    const session = await conn.startSession();
    await session.withTransaction(async () => {
      const projectId = req.params.id;
      const ids = req.body.idChannelsDelete;
      const project = await Project.findOne({
        _id: projectId,
        users: { $elemMatch: { userId: req.user._id, role: "ADMIN" } },
      });
      if (!project) return errorHandler(res, err.NOT_AUTHORIZED);
      const z = ids?.every((id) => {
        const ind = project.channels.indexOf(id);
        if (ind > -1) {
          project.channels.splice(ind, 1);
          return true;
        }
        errorHandler(res, err.CHANNEL_NOT_FOUND_IN_PROJECT, {
          message: "Không tìm thấy thông tin channel " + id + " trong project",
        });
      });
      if (!z) return;
      await project.save({ new: true, session });
      await Channel.updateMany(
        { _id: { $in: ids } },
        { $pull: { projects: projectId } },
        { session }
      );
      return successHandler(res, { project });
    });
    session.endSession();
  } catch (error) {
    console.log("___error___",error);;
    return errorHandler(res, error);
  }
};

exports.sendMessage = async (req, res, next) => {
  try {
    // ========== Test ==================
    // await sendMessage(
    //     '6065039103566611',
    //     'EAAlK610BXFsBALgeSMZB2wbmZC0WAjzya5kp5r86q1s501KPApSZA62svgTlXsZBjmghFuxfiZB0nlWaezmSb6ZACqu39KflmvVxogdSPKtIwDWT06klaG4jilfCYVyCZBCCZBPHZAZAZBuZClqWWeShbU34OvujdkrePIDknLeW0NWZCZCaIfVaXuMSw8K3Sd575znhoZD',
    //     'test')
    // await getUserProfile(
    //     '6065039103566611',
    //     'EAAlK610BXFsBALgeSMZB2wbmZC0WAjzya5kp5r86q1s501KPApSZA62svgTlXsZBjmghFuxfiZB0nlWaezmSb6ZACqu39KflmvVxogdSPKtIwDWT06klaG4jilfCYVyCZBCCZBPHZAZAZBuZClqWWeShbU34OvujdkrePIDknLeW0NWZCZCaIfVaXuMSw8K3Sd575znhoZD')
    // await getListConversations('107613858079257','EAAlK610BXFsBALgeSMZB2wbmZC0WAjzya5kp5r86q1s501KPApSZA62svgTlXsZBjmghFuxfiZB0nlWaezmSb6ZACqu39KflmvVxogdSPKtIwDWT06klaG4jilfCYVyCZBCCZBPHZAZAZBuZClqWWeShbU34OvujdkrePIDknLeW0NWZCZCaIfVaXuMSw8K3Sd575znhoZD')
    // await getListMessage('t_3035340680080142','EAAlK610BXFsBALgeSMZB2wbmZC0WAjzya5kp5r86q1s501KPApSZA62svgTlXsZBjmghFuxfiZB0nlWaezmSb6ZACqu39KflmvVxogdSPKtIwDWT06klaG4jilfCYVyCZBCCZBPHZAZAZBuZClqWWeShbU34OvujdkrePIDknLeW0NWZCZCaIfVaXuMSw8K3Sd575znhoZD')
    await getMessage(
      "m_ImQVFY-8U1ir1v2fH0yWOq02UcJu6tIX7cCEdh-KfqK941OzVk7rvmY8aCkOVKl36ynZV0OsyMPCmQO5XyPFVA",
      "EAAlK610BXFsBALgeSMZB2wbmZC0WAjzya5kp5r86q1s501KPApSZA62svgTlXsZBjmghFuxfiZB0nlWaezmSb6ZACqu39KflmvVxogdSPKtIwDWT06klaG4jilfCYVyCZBCCZBPHZAZAZBuZClqWWeShbU34OvujdkrePIDknLeW0NWZCZCaIfVaXuMSw8K3Sd575znhoZD"
    );
  } catch (error) {}
};
