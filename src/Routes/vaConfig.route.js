const express = require("express");
const validate = require("../Middleware/validators.middleware");
const {
  getVaConfig,
  createVaConfig,
  updateAutoRepLy,
} = require("../Controllers/vaConfig.controllers");
const router = express.Router();
const auth = require("../Middleware/auth.middleware");
const vaConfigMiddleware = require("../Middleware/vaConfig.middleware");
router.get("/get_conf/:id", auth, getVaConfig);
router.post(
  "/create",
  auth,
  vaConfigMiddleware.preCheckVaConfig,
  auth,
  validate.vaConfig.create,
  createVaConfig
);
router.post(
  "/update-auto-repLy/:id",
  auth,
  validate.vaConfig.updateAutoRepLy,
  updateAutoRepLy
);

module.exports = router;
