const express = require("express");
const intentController = require("../Controllers/intent.controllers");
const router = express.Router();
const auth = require("../Middleware/auth.middleware");
const validate = require("../Middleware/validators.middleware");

router.get("/", /*auth,*/ intentController.findAll);
router.post(
  "/",
  /*auth,*/ validate.inputIntentAndEntity,
  intentController.create
);
router.put(
  "/:id",
  /*auth,*/
  validate.inputIntentAndEntity,
  intentController.update
);
router.delete("/:id", /*auth,*/ intentController.delete);

module.exports = router;
