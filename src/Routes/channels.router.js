const express = require("express");
const { configLiveChat, getChannelLiveChatByProjectId } = require("../Controllers/channel.controllers");
const router = express.Router();
const auth = require("../Middleware/auth.middleware");
const validate = require("../Middleware/validators.middleware");
router.post("/config_live_chat", auth,validate.channel.configLiveChat, configLiveChat);
router.get("/get_channel_live_chat_by_projectId/:id", getChannelLiveChatByProjectId);

// getChannelLiveChatByProjectId
module.exports = router;