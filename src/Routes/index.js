const express = require("express");
const authRoute = require("./auth.routes");
const projectRoutes = require("./project.routes");
const usersRoute = require("./users.routes");
const docsRoute = require("./docs.routes");
const messageRoute = require("./message.route");
const vaConfigRoute = require("./vaConfig.route");
const channelConfigRoute = require("./channels.router");
const intentConfigRoute = require("./intent.routers");
const entityConfigRoute = require("./entity.routers");


const stringeex = require("./stringeex.routes");
const conversationRoute = require("./conversation.routes");
const router = express.Router();

const defaultRoutes = [
  {
    path: "/auth",
    route: authRoute,
  },
  {
    path: "/projects",
    route: projectRoutes,
  },
  {
    path: "/users",
    route: usersRoute,
  },
  {
    path: "/message",
    route: messageRoute,
  },
  {
    path: "/conversations",
    route: conversationRoute,
  },
  {
    path: "/va_config",
    route: vaConfigRoute,
  },
  {
    path: "/stringeex",
    route: stringeex,
  },
  {
    path: "/channels",
    route: channelConfigRoute,
  },
  {
    path: "/intents",
    route: intentConfigRoute,
  },
  {
    path: "/entities",
    route: entityConfigRoute,
  },
];

const devRoutes = [
  // routes available only in development mode
  {
    path: "/docs",
    route: docsRoute,
  },
];

defaultRoutes.forEach((route) => {
  router.use(route.path, route.route);
});

const env = "development";

/* istanbul ignore next */
if (env === "development") {
  devRoutes.forEach((route) => {
    router.use(route.path, route.route);
  });
}

module.exports = router;
