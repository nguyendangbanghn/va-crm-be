const express = require("express");
const { getToken } = require("../Controllers/stringeex.controllers");
const router = express.Router();

router.post("/get_token", getToken);

module.exports = router;
