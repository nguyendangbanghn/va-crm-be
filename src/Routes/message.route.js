const express = require("express");
const router = express.Router();

const auth = require("../Middleware/auth.middleware");
const projectMiddleware = require("../Middleware/project.middleware");
const validate = require("../Middleware/validators.middleware");
const {
  create,
  getMessageByConversation,
  actionMessageHandler,
  exportCsv,
  updateEntity,
  updateIntent,
  searchMessages,
} = require("../Controllers/message.controller");
const messageMiddleware = require("../Middleware/message.middleware");
router.post(
  "/send_msg",
  messageMiddleware.preCheckAiMessage,
  validate.message.create,
  create
);
router.get("/get_message_by_conversation", auth, getMessageByConversation);
router.post(
  "/action_message_handler",
  auth,
  validate.message.actionMessageHandler,
  actionMessageHandler
);
router.get("/export-csv", /*auth,*/ exportCsv);

router.post(
  "/update-entity/:id",
  validate.message.updateEntity,
  /*auth,*/ updateEntity
);
router.post(
  "/update-intent/:id",
  validate.message.updateIntent,
  /*auth,*/ updateIntent
);

router.post(
  "/search-messages",
  validate.message.searchMessages,
  /*auth,*/ searchMessages
);

module.exports = router;
