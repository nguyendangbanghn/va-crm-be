const express = require("express");
const router = express.Router();
const entityController = require("../Controllers/entity.controllers");
const auth = require("../Middleware/auth.middleware");
const validate = require("../Middleware/validators.middleware");

router.get("/", /*auth,*/ entityController.findAll);
router.post(
  "/",
  /*auth,*/ validate.inputIntentAndEntity,
  entityController.create
);
router.put(
  "/:id",
  /*auth,*/
  validate.inputIntentAndEntity,
  entityController.update
);
router.delete("/:id", /*auth,*/ entityController.delete);

module.exports = router;
