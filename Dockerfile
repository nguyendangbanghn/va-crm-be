# Dockerfile  
# FROM node:14
# WORKDIR /app  
# COPY package.json /app  
# RUN npm install  
# COPY . /app  

# EXPOSE 8026  
# CMD cmd node app.js
# Dockerfile  
FROM node:14-alpine
WORKDIR /app  

COPY package.json .

COPY . .

RUN npm install 

EXPOSE 8026

CMD ["npm","run", "start"]
